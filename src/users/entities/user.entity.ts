import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  fullName: string;
  // roles: ('admin' | 'user')[];

  @Column()
  gender: 'male' | 'female' | 'others';
}
